export default {
  server: {
    port: 9000
  },
  head: {
    title: 'client',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  css: [],
  plugins: [
    'plugins/gConfig',
    'plugins/gApi',
  ],
  router: {
  },
  components: true,
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/tailwindcss',
    '@nuxtjs/moment',    
  ],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/svg-sprite',
  ],
  axios: {
    baseURL: '',
  },
  build: {},
  svgSprite: {
    input: '~/static/icons/'
  },
  moment: {
    defaultTimezone: 'Asia/Seoul',
  },
  tailwindcss: {
    configPath: '~/tailwind.config.js',
    cssPath: 'static/style/tailwind.css',
    purgeCSSInDev: false,
    exposeConfig: false,
  },
}
