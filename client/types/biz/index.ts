export enum RequestMethod {
  ALL = 'all',
  GET = 'get',
  POST = 'post',
  DELETE = 'delete',
  PATCH = 'patch',
  OPTIONS = 'options',
  HEAD = 'head'
}

export enum ResponseCode {
  OK = 200
}
export interface IResponse {
  status: string
  data: any
}