export interface ITask {
  
  status: TaskStatus  
  /**
   * 단체명
   */
  orgNm: string
  /**
   * 주문자명
   */
  orderer: string
  /**
   * 직급
   */
  position: string
  /**
   * 부서명
   */
  dept: string
  /**
   * 핸드폰번호
   */
  mobile: string
  /**
   * 전화번호
   */
  tel: string
  /**
   * 이메일
   */
  email: string
  /**
   * 상품명
   */
  productNm: string
  /**
   * 문의 수량
   */
  quantity: number
  /**
   * 납기일시
   */
  dueAt: Date
  /**
   * 문의일시
   */
  reqAt: Date
  /**
   * 최초 응답일
   */
  resAt: Date
  /**
   * 담당자 아이디
   */
  resUserId: string
  /**
   * 추가설명
   */
  desc: string
}

export enum TaskStatus {
  WAIT = 'WT',
  READY = 'RD',
  STEP1 = 'S1',
  STEP2 = 'S2',
  STEP3 = 'S3',
  STEP4 = 'S4',
  STEP5 = 'S5'
};