import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { Plugin } from '@nuxt/types'

/**
 * Vue 인스턴스
 */
declare module 'vue/types/vue' {
  interface Vue {
    $axios: NuxtAxiosInstance
  }
}
/**
 * Nuxt 전역 옵션
 */
declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $axios: NuxtAxiosInstance
  }
}

/**
 * Store 
 */
declare module 'vuex/types/index' {
  interface Store<S> {
    $axios: NuxtAxiosInstance
  }
}

const extendsPlugin: Plugin = (context, inject) => {
  inject('axios', context.$axios)
}

export default extendsPlugin
