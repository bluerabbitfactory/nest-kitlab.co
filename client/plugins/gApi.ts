
import {initialApiHelper, ApiHelper, IApiHelper } from '~/helper/ApiHelper'

  /**
 * Vue 인스턴스
 */
declare module 'vue/types/vue' {
  interface Vue {
    $Api: IApiHelper
  }
}
/**
 * Nuxt 전역 옵션
 */
declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $Api: IApiHelper
  }
}

/**
 * Store 
 */
declare module 'vuex/types/index' {
  interface Store<S> {
    $Api: IApiHelper
  }
}

export default function(context: any, inject: any) {
  // 공통 API
  const apiHelper: IApiHelper = new ApiHelper(context)
  context.$Api = apiHelper
  inject('Api', apiHelper)
  initialApiHelper(context)
}