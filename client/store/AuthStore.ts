import Vuex, {Store} from 'vuex'

import {
    Module,
    Action,
    Mutation,
    VuexModule,
    getModule
  } from 'vuex-module-decorators'

import { IResponse, ResponseCode } from '../types/biz'
import { $Api } from '~/helper/ApiHelper'

export interface IAuthStore {
  isLogin: boolean
  doAdminLogin(userId: string, password: string): void
}

const store = new Vuex.Store<IAuthStore>({})
@Module({
  name: 'authStore',
  dynamic: true,
  store: store
})
export class AuthStore extends VuexModule implements IAuthStore {
  isLogin: boolean = false

  @Mutation
  setIsLogin(_isLogin: boolean) {
    this.isLogin = _isLogin
  }

  @Action
  async doAdminLogin(param: any) {
    try {
      console.log(`###############`)
      const res: IResponse | any = await $Api.post('/api/v1/admin/sign', param)
      if(res.status == ResponseCode.OK) {
        this.setIsLogin(true)
      }
    } catch (error) {
      console.error(error)
    }
  }
}
export default getModule(AuthStore)