import Vuex, {Store} from 'vuex'
import { Module, Action, Mutation, VuexModule, getModule } from 'vuex-module-decorators'
import { $Api } from '~/helper/ApiHelper'
import { IResponse, ResponseCode } from '../types/biz'

export interface IAdminStore {
  gTopMenuActive: boolean 
  gFooterActive: boolean
  gPageOverlay: boolean
  menuList: any[]
}

// 어드민 스토어
const store = new Vuex.Store<IAdminStore>({
})

@Module({
  name: 'adminStore',
  dynamic: true,
  store: store
})
export class AuthStore extends VuexModule implements IAdminStore {
  gTopMenuActive: boolean = false
  gFooterActive: boolean = false
  gPageOverlay: boolean = false
  menuList: any[] = []

  @Mutation
  setGTopMenuActive(flag: boolean) {
    this.gTopMenuActive = flag
  }
  @Mutation
  setGFooterActive(flag: boolean) {
    this.gFooterActive = flag
  }
  @Mutation
  setGPageOverlay(flag: boolean) {
    this.gPageOverlay = flag
  }
  @Mutation
  setMenuList(menuList: any[]) {
    this.menuList = menuList 
  }

  /**
   * 어드민 메뉴리스트 조회
   */
  @Action
  async getAdminMenuTree () {
    try {
      const res: any = await $Api.get('/api/v1/admin/menu')
      if(res.status == ResponseCode.OK) {
        this.setMenuList(res.data)
      }
    } catch (error) {
      console.error(error)
    }
  }
}
export default getModule(AuthStore)
