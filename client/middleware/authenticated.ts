import { Context } from "@nuxt/types"

/**
 * 권한체크 예외 대상인지 체크 한다.
 * @param path 
 */
export const isExcludeAuthPath = (path: string) => {
  return [
    '/admin/external/swit',
  ].find(p => p === path )
}

export default  function( context: Context) {
  if(isExcludeAuthPath(context.route.path)) {
    return
  }
  // cookie에 토큰여부 체크
  const token = context.app.$cookies.get('KIT_TOKEN')
  if(!token) {
    if(context.route.path.startsWith('/admin')) {
      // admin for kitlab 
      if(context.route.path !== '/admin/login') {
        return context.redirect('/admin/login')
      }
    } else {
      // for kitlab
      return context.redirect('/survey')
    }
  }
}