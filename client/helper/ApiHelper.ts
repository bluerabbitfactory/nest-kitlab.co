import { AxiosRequestConfig, AxiosResponse } from 'axios'
import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { IResponse } from '~/types/biz'

export let $Api: IApiHelper

export interface IApiHelper { 
  post(url: string, param?: any, reqConfig?: AxiosRequestConfig): Promise<AxiosResponse<IResponse>> 
  get(url: string, param?: any, reqConfig?: AxiosRequestConfig): Promise<AxiosResponse<IResponse>> 
  put(url: string, param?: any, reqConfig?: AxiosRequestConfig): Promise<AxiosResponse<IResponse>> 
}

export class ApiHelper implements IApiHelper {
  baseAxios: NuxtAxiosInstance
  
  constructor(context: any) {
    // 요청에 토큰 설정
    context.$axios.onRequest((config: any) => {
      config.headers.common['Authorization'] = `Bearer ${context.$cookies.get('KIT_TOKEN')}`
    })
    
    context.$axios.onError((error: any) => {
      const code = parseInt(error.response && error.response.status)
      if (code === 401) {
        context.redirect('/401')
      }
    })
    this.baseAxios = context.$axios
  }
  /**
   * post 
   */
  post = (url: string, param?: any, reqConfig?: AxiosRequestConfig): Promise<AxiosResponse<IResponse>> => {
    return this.baseAxios.$post(url, param, reqConfig)
  }
  /**
   * put 
   */
  put = (url: string, param?: any, reqConfig?: AxiosRequestConfig): Promise<AxiosResponse<IResponse>> => {
    return this.baseAxios.$put(url, param, reqConfig)
  }
  /**
   * get
   */
  get = (url: string, reqConfig?: AxiosRequestConfig): Promise<AxiosResponse<IResponse>> => {
    return this.baseAxios.$get(url)
  }
}
/**
 * util 형태로 axios 활용을 위함
 * @param context 
 */
export const initialApiHelper = (context: any) => {
  $Api = new ApiHelper(context)
}