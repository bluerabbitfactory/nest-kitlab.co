import { Injectable, CanActivate, ExecutionContext, Logger } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserRole } from '../../../types';

@Injectable()
export class RolesGuard implements CanActivate {
  private readonly logger = new Logger(`### ${RolesGuard.name} ###`)
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.getAllAndOverride<UserRole[]>('roles', [
      context.getHandler(),
      context.getClass(),
    ])
    if (!roles) {
      return true
    }
    this.logger.log(`need role: ${roles.join(',')}`)
    const request = context.switchToHttp().getRequest()
    const { user } = request.user
    this.logger.log(`user has role: ${user?.role}`)
    // 롤 체크
    return roles.some(role => user?.role === role)
  }ß
}