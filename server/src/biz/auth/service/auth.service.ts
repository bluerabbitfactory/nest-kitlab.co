import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserDto } from '../../user/dto/user.dto';
const bcrypt = require('bcrypt');

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}
  genJWT(user: UserDto): Promise<string> { 
    // TODO: jwt에 권한롤 추가하자!
    return this.jwtService.signAsync({
      userId: user.userId,
      userName: user.userName,
      role: user.role
    })
  }
  hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, Number(process.env.CRYPT_SALTROUNTS))
  }
  comparePassword(password: string, passwordHash: string): Promise<boolean>  {
    return bcrypt.compare(password, passwordHash)
  }
}
