import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import { Roles } from '../../auth/decorator/roles.decorator';
import { JwtAuthGuard } from '../../auth/guards/jwt.guard';
import { RolesGuard } from '../../auth/guards/roles.guard';
import { UserRole } from '../../../types';
import { LoginDto } from '../dto/login.dto';
import { ResponseUserDto } from '../dto/response-user.dto';
import { UserDto } from '../dto/user.dto';
import { UserEntity } from '../entity/user.entity';
import { UserService } from '../service/user.service';

@Controller('user')
export class UserController {
  
  constructor(private userService: UserService) { 

  }

  @Post("/")
  create(@Body() user: UserDto): Promise<ResponseUserDto> {
    return this.userService.create(user)
  }

  @Get('/')
  @Roles(UserRole.MASTER, UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  findAll(): Promise<ResponseUserDto[]| UserEntity[]>{
    return this.userService.findAll();
  }

  @Get('/:id')
  async findOne(@Param('id') userId: string): Promise<ResponseUserDto | UserEntity>{
    return new ResponseUserDto(await this.userService.findOne(userId));
  }
  
  @Post("/login")
  login(@Body() login: LoginDto): Promise<ResponseUserDto> {
    return this.userService.login(login)
  }
}
