import { ConflictException, Injectable, UnauthorizedException, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { ResponseUserDto } from '../dto/response-user.dto';
import { UserDto } from '../dto/user.dto';
import { UserEntity } from '../entity/user.entity';
import { AuthService } from '../../auth/service/auth.service';
import { LoginDto } from '../dto/login.dto';

@Injectable()
export class UserService {
  private readonly logger = new Logger(`### ${UserService.name} ###`)
  constructor( 
    @InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity> ,
    private authService: AuthService
  ){}
  /**
   * 사용자 등록
   * @param user 사용자 정보
   */
  async create (user: UserDto): Promise<ResponseUserDto> {
    return new Promise(async (reslove, reject) => {
      try {
        // 사용자 중복체크 
        if(await this.findOne(user.userId)) {
          throw new ConflictException(`[${user.userId}] is already exists.`)
        }
        user.password = await this.authService.hashPassword(user.password)
        this.logger.debug(`hash string of password: ${user.password}`)
        reslove( new ResponseUserDto( await this.userRepository.save(user) ))
      } catch (error) {
        this.logger.error(error)
        reject(error)
      }     
    })
  }
  /**
   * 사용자 로그임 서비스
   * @param login 로그인정보
   */
  async login (login: LoginDto): Promise<ResponseUserDto> {
    return new Promise(async (reslove, reject) => {
      try {
        // 사용자 체크
        const user: UserDto = await this.findOne(login.userId)
        if(!user) {
          throw new UnauthorizedException(`[${login.userId}] is not a member.`)
        }
        if(this.authService.comparePassword(login.password, user.password)){
          // 인증토큰 생성
          const token = await this.authService.genJWT(user)
          reslove( new ResponseUserDto( user, token )) 
        }
        throw new UnauthorizedException(`Please check your password.`)
      } catch (error) {
        reject(error)
      }     
    })
  }
  /**
   * 특정 사용자 조회 by 사용자 아이디
   * @param id 사용자 아이디
   */
  findOne(id: string):  Promise<UserDto | UserEntity> {
    return this.userRepository.findOne({ where: {userId: id}})
  }
  /**
   * 사용자 전체 조회
   */
  findAll(): Promise<ResponseUserDto[] | UserEntity[]> {
    return new Promise(async(resolve, reject) => {
      const users: UserEntity[] = await this.userRepository.find()
      resolve( users.map(user => new ResponseUserDto( user))) 
    }) 
  }
  /**
   * 사용자 삭제
   * @param id 사용자 아이디
   */
  deleteOne(id: string): Promise<DeleteResult> {
    return this.userRepository.delete(id)
  }
}

