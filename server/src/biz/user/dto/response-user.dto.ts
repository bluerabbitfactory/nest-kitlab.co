import { UserRole, UserStatus, YnFlag } from "../../../types"
import { UserEntity } from "../entity/user.entity";

export class ResponseUserDto {
  
  userId: string
  userName: string
  email: string
  status: UserStatus
  deletedAt?: Date
  role: UserRole
  authToken?: string
  constructor(userEntity: UserEntity | any, authToken?: string) {
    this.userId = userEntity.userId
    this.userName = userEntity.userName
    this.email = userEntity.email
    this.status = userEntity.status
    this.deletedAt = userEntity.deletedAt
    this.role = userEntity.role
    if(authToken){
      this.authToken = authToken
    }
  }
}
