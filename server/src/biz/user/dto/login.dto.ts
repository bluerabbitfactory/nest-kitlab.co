import { Length } from "class-validator"

export class LoginDto {
  
  @Length(2, 30)
  userId: string

  @Length(7, 20)
  password: string
  
}
