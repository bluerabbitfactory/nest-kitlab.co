import { IsEmail, IsEnum, Length } from "class-validator"
import { UserRole, UserStatus, YnFlag } from "../../../types"

export class UserDto {
  
  @Length(2, 30)
  userId: string

  @Length(7, 20)
  password: string
  
  @Length(2, 50)
  userName: string
  
  @IsEmail()
  email: string
  
  @IsEnum(UserRole)
  role?: UserRole

  @IsEnum(UserStatus)
  status?: UserStatus

  deletedAt?: Date
}
