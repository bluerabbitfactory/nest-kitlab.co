import { UserRole, UserStatus } from '../../../types'
import {Entity, Column, PrimaryGeneratedColumn, Unique, CreateDateColumn, BeforeInsert, BeforeUpdate, ManyToOne, JoinColumn, BaseEntity} from "typeorm"
import { Exclude } from 'class-transformer'
      
@Entity('kit_user')
@Unique(['userId'])
export class UserEntity extends BaseEntity{

  @PrimaryGeneratedColumn('uuid')
  id: string
  
  @Column({ length: 30, comment: "아이디" })
  userId: string
  
  @Exclude()
  @Column({ length: 200, comment: "패스워드" })
  password: string
  
  @Column({ length: 120, comment: "사용자명" })
  userName: string

  @Column({ length: 50, comment: "이메일" })
  email: string

  @Column({ 
    type: 'enum', 
    enum: UserRole, 
    default: UserRole.USER,
    comment: "권한"
  })
  role: UserRole

  @Column({ 
    type: 'enum', 
    enum: UserStatus, 
    default: UserStatus.ALIVE,
    comment: "상태코드"
  })
  status: UserStatus
  
  @Column({ comment: "삭제일", default: null })
  deletedAt: Date

  /**********************************************
   * 테이블 컬럼 공통
   **********************************************/
  
  @Column({ type: "timestamp", comment: "최근갱신일시" })
  @CreateDateColumn()
  modifyAt!: Date

  @Column({ type: "timestamp", comment: "생성일시" })
  @CreateDateColumn()
  createAt!: Date

  @BeforeInsert()
  setCreateDate() {
    this.createAt = new Date()
  }

  @BeforeUpdate()
  setModifyDate() {
    this.modifyAt = new Date()
  }
}