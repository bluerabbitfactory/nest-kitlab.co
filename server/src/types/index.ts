export enum UserStatus {
  ALIVE = 'A',
  DELETED = 'D'
}

export enum UserRole {
  MASTER = 'master',
  ADMIN = 'admin',
  USER = 'user'
}

export enum YnFlag { 
  Y = 'Y',
  N = 'N' 
}


